---
title: "Dengue and weather variables"
author: "Raphael Saldanha"
date: "2023-02-23"
output: html_document
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Packages

```{r}
library(tidyverse)
library(arrow)
library(fpp3)
```

## Data

```{r}
dengue <- open_dataset("../dengue-data/parquet_aggregated/dengue_md.parquet")

pr <- open_dataset("../brclim/parquet/brdwgd/pr.parquet")
tmax <- open_dataset("../brclim/parquet/brdwgd/tmax.parquet")
tmin <- open_dataset("../brclim/parquet/brdwgd/tmin.parquet")
u2 <- open_dataset("../brclim/parquet/brdwgd/u2.parquet")
```

## Case study: Rio de Janeiro, RJ

```{r}
dengue_rj <- dengue %>%
  filter(mun == 330455) %>%
  select(-mun) %>%
  rename(cases = freq) %>%
  collect()

pr_rj <- pr %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "pr_sum") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(rain = value)

tmax_rj <- tmax %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmax_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmax = value)

tmin_rj <- tmin %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmin_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmin = value)

u2_rj <- u2 %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "u2_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(u2 = value)

res <- inner_join(dengue_rj, pr_rj) %>%
  inner_join(tmax_rj) %>%
  inner_join(tmin_rj) %>%
  inner_join(u2_rj) 
```


### Aggregate by month

```{r}
res_m <- res %>%
  as_tsibble(
    key = c(rain, tmax, tmin, u2),
    index = date
  ) %>%
  index_by(year_month = ~ yearmonth(.)) %>%
  summarise(
    cases = sum(cases, na.rm = TRUE),
    rain = sum(rain, na.rm = TRUE),
    tmax = mean(tmax, na.rm = TRUE),
    tmin = mean(tmin, na.rm = TRUE),
    u2 = mean(u2, na.rm = TRUE)
  )
```

## Visualization

### Time series plots

```{r}
autoplot(res_m, cases)
autoplot(res_m, rain)
autoplot(res_m, tmax)
autoplot(res_m, tmin)
autoplot(res_m, u2)
```

### Seasonal plots

```{r}
res_m %>%
  gg_season(cases)

res_m %>%
  gg_season(rain)

res_m %>%
  gg_season(tmax)

res_m %>%
  gg_season(tmin)

res_m %>%
  gg_season(u2)
```

### Subseries plot

```{r}
res_m %>%
  gg_subseries(cases)

res_m %>%
  gg_subseries(rain)

res_m %>%
  gg_subseries(tmax)

res_m %>%
  gg_subseries(tmin)
               
res_m %>%
  gg_subseries(u2)
```

### Lag plots

```{r}
res_m %>%
  gg_lag(cases, geom = "point", lags = 1:12)

res_m %>%
  gg_lag(rain, geom = "point", lags = 1:12)

res_m %>%
  gg_lag(tmax, geom = "point", lags = 1:12)

res_m %>%
  gg_lag(tmin, geom = "point", lags = 1:12)

res_m %>%
  gg_lag(u2, geom = "point", lags = 1:12)
```

## Correlation analysis

### Autocorrelation

```{r}
res_m %>%
  ACF(cases) %>%
  autoplot()

res_m %>%
  ACF(rain) %>%
  autoplot()

res_m %>%
  ACF(tmax) %>%
  autoplot()

res_m %>%
  ACF(tmin) %>%
  autoplot()

res_m %>%
  ACF(u2) %>%
  autoplot()
```


### Partial Autocorrelation

```{r}
res_m %>%
  PACF(cases) %>%
  autoplot()

res_m %>%
  PACF(rain) %>%
  autoplot()

res_m %>%
  PACF(tmax) %>%
  autoplot()

res_m %>%
  PACF(tmin) %>%
  autoplot()

res_m %>%
  PACF(u2) %>%
  autoplot()
```


### Cross-correlation

```{r}
res_m %>%
  CCF(cases, rain) %>%
  autoplot()

res_m %>%
  CCF(cases, tmax) %>%
  autoplot()

res_m %>%
  CCF(cases, tmin) %>%
  autoplot()

res_m %>%
  CCF(cases, u2) %>%
  autoplot()
```


## Time series decomposition

### X-11 method

```{r}
res_m %>%
  model(x11 = X_13ARIMA_SEATS(cases ~ x11())) %>%
  components() %>%
  autoplot()
```

### SEATS

```{r}
res_m %>%
  model(seats = X_13ARIMA_SEATS(cases ~ seats())) %>%
  components() %>%
  autoplot()
```

### STL (loess)

```{r}
res_m %>%
  model(stl = STL(cases)) %>%
  components() %>%
  autoplot()
```


## Models

### Linear regression

```{r}
fit_lm <- res_m %>%
  model(TSLM(cases ~ rain + tmax + tmin + u2))

fit_lm%>%
  report()
```

```{r}
glance(fit_lm) %>%
  select(adj_r_squared, CV, AIC, AICc, BIC)
```


```{r}
fit_lm %>%
  augment() %>%
  ggplot(aes(x = year_month)) +
  geom_line(aes(y = cases, colour = "Data")) +
  geom_line(aes(y = .fitted, colour = "Fitted"))
```

```{r}
fit_lm %>%
  gg_tsresiduals()
```

### ARIMA

```{r}
arima_fit <- res_m %>%
  model(
    univariate = ARIMA(cases),
    multivariate_1 = ARIMA(cases ~ rain),
    multivariate_2 = ARIMA(cases ~ rain + tmax),
    multivariate_3 = ARIMA(cases ~ rain + tmax + tmin),
    multivariate_4 = ARIMA(cases ~ rain + tmax + tmin + u2)
  )

````

```{r}
glance(arima_fit) %>%
  select(.model:BIC) %>%
  arrange(AICc)
```

## DTW

```{r}
library(dtw)

alignment <- dtw(
  x = res_m$cases, 
  y = res_m$rain, 
  keep = TRUE
)
plot(alignment, type = "threeway")
```

```{r}
alignment <- dtw(
  x = res_m$cases, 
  y = res_m$tmax, 
  keep = TRUE
)
plot(alignment, type = "threeway")
```

```{r}
alignment <- dtw(
  x = res_m$cases, 
  y = res_m$tmin, 
  keep = TRUE
)
plot(alignment, type = "threeway")
```