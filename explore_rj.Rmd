---
title: "Dengue and weather variables"
author: "Raphael Saldanha"
date: "2023-02-20"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Packages

```{r}
library(tidyverse)
library(arrow)
library(timetk)
library(dtw)
interactive = FALSE
```

## Data

```{r}
dengue <- open_dataset("../dengue-data/parquet_aggregated/dengue_md.parquet")

pr <- open_dataset("../brclim/parquet/pr.parquet")
tmax <- open_dataset("../brclim/parquet/tmax.parquet")
tmin <- open_dataset("../brclim/parquet/tmin.parquet")
u2 <- open_dataset("../brclim/parquet/u2.parquet")
```

## Case study: Rio de Janeiro, RJ

```{r}
dengue_rj <- dengue %>%
  filter(mun == 330455) %>%
  select(-mun) %>%
  rename(cases = freq) %>%
  collect()

pr_rj <- pr %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "pr_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(rain = value)

tmax_rj <- tmax %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmax_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmax = value)

tmin_rj <- tmin %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmin_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmin = value)

u2_rj <- u2 %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "u2_mean") %>%
  filter(code_muni == 3304557) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(u2 = value)

res <- inner_join(dengue_rj, pr_rj) %>%
  inner_join(tmax_rj) %>%
  inner_join(tmin_rj) %>%
  inner_join(u2_rj) 
```


### Aggregate by month

```{r}
res_m <- res %>%
  pivot_longer(!date) %>%
  group_by(name) %>%
  summarise_by_time(date, .by = "month", value = mean(value)) %>%
  pivot_wider()
```

### Plot time series


```{r, fig.height=20}
res_m %>%
  pivot_longer(!date) %>%
  group_by(name) %>%
  plot_time_series(date, value, 
                   .facet_ncol = 1, .facet_scales = "free",
                   .smooth = FALSE,
                   .interactive = interactive)
```

### Decompose cases time series

```{r, fig.height=20}
res_m %>%
  pivot_longer(!date) %>%
  filter(name == "cases") %>%
  plot_stl_diagnostics(
    .date_var = date, .value = value,
    .interactive = interactive
  )
```

### Correlations

```{r, fig.height=10}
res_m %>%
   plot_acf_diagnostics(
        date, cases,
        .ccf_vars = c(rain, tmax, tmin, u2),
        .lags = "20 months",
        .interactive = interactive
    )
```

### DTW

```{r}
alignment <- dtw(
  x = res_m$rain, 
  y = res_m$cases, 
  keep = TRUE
)
plot(alignment, type = "threeway", main = "Cases (reference) and rain (query)")
```









## Case study: Brasília, DF

```{r}
dengue_bsb <- dengue %>%
  filter(mun == 530010) %>%
  select(-mun) %>%
  rename(cases = freq) %>%
  collect()

pr_bsb <- pr %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "pr_mean") %>%
  filter(code_muni == 5300108) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(rain = value)

tmax_bsb <- tmax %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmax_mean") %>%
  filter(code_muni == 5300108) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmax = value)

tmin_bsb <- tmin %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "Tmin_mean") %>%
  filter(code_muni == 5300108) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(tmin = value)

u2_bsb <- u2 %>%
  filter(date >= as.Date("2007-01-01")) %>%
  filter(name == "u2_mean") %>%
  filter(code_muni == 5300108) %>%
  select(-code_muni, -name) %>%
  collect() %>%
  rename(u2 = value)

res <- inner_join(dengue_bsb, pr_bsb) %>%
  inner_join(tmax_bsb) %>%
  inner_join(tmin_bsb) %>%
  inner_join(u2_bsb) 
```




### Aggregate by month

```{r}
res_m <- res %>%
  pivot_longer(!date) %>%
  group_by(name) %>%
  summarise_by_time(date, .by = "month", value = mean(value)) %>%
  pivot_wider()
```

### Plot time series

```{r, fig.height=20}
res_m %>%
  pivot_longer(!date) %>%
  group_by(name) %>%
  plot_time_series(date, value, 
                   .facet_ncol = 1, .facet_scales = "free",
                   .smooth = FALSE,
                   .interactive = interactive)
```

### Decompose cases time series

```{r, fig.height=20}
res_m %>%
  pivot_longer(!date) %>%
  filter(name == "cases") %>%
  plot_stl_diagnostics(
    .date_var = date, .value = value,
    .interactive = interactive
  )
```

### Correlations

```{r, fig.height=10}
res_m %>%
   plot_acf_diagnostics(
        date, cases,
        .ccf_vars = c(rain, tmax, tmin, u2),
        .lags = "20 months",
        .interactive = interactive
    )
```

### DTW test

```{r}
alignment <- dtw(
  x = res_m$cases, 
  y = res_m$rain, 
  keep = TRUE
)
plot(alignment, type = "threeway")
```









